# README #

3D Game made in study subject "Game Design" in Master study

### What is this repository for? ###

* 3D Endless Runner - kind of "Crash Bandicoot Polar Bear" Levels
* Made in Unity3D
* Script language: C#
* Items (bones) made in blender
* Assets are imported from Unity Asset Store
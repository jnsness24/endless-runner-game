﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockCamera : MonoBehaviour {

    public float CameraLockY = 0.69f;

    private void LateUpdate()
    {
        //for later implementations
        //transform.position = new Vector3(Mathf.Clamp(transform.position.x,-5.0f,15.0f), CameraLockY, transform.position.z);
        transform.position = new Vector3(transform.position.x, CameraLockY, transform.position.z);
    }
}

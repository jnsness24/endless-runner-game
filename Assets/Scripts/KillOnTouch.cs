﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillOnTouch : MonoBehaviour {

    protected static Animator anim;
    
    protected static GameObject playerObject;
    protected static Transform playerTransform;
    private static GameObject respawnPoint;
    private GameObject eventSystem;
    private ActiveSwitcherDie dieMenuScript;

    protected static PlayerMove moveScript;


    void Start()
    {
        eventSystem = GameObject.Find("EventSystem");
        respawnPoint = GameObject.Find("RespawnPoint");
        playerObject = GameObject.Find("SimpleCorgi");

        dieMenuScript = eventSystem.GetComponent<ActiveSwitcherDie>();
        playerTransform = playerObject.GetComponent<Transform>();
        moveScript = playerTransform.GetComponent<PlayerMove>();
        anim = playerTransform.GetComponent<Animator>();

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
        
            anim.SetTrigger("die");
            moveScript.freezeSwitch = true;
            moveScript.lives -= 1;
            StartCoroutine(dieAnimation());        
        }

    }

    IEnumerator dieAnimation()
    {

        yield return new WaitForSeconds(1.5f);

        if (moveScript.lives == 0)
        {
            dieMenuScript.showDieScreen();

        }
        else
        {
            playerObject.transform.position = respawnPoint.transform.position;
            moveScript.freezeSwitch = false;
        }
        //Application.LoadLevel(Application.loadedLevel);
    }

}

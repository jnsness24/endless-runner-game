﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundaryTurnMobAround : MonoBehaviour
{

    public Transform MobSideRunner;
    public float RotateDegree;
    MobSideRunnerMovement enemyScript;

    private void Start()
    {
        enemyScript = MobSideRunner.GetComponent<MobSideRunnerMovement>();     
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "enemy")
        {
            MobSideRunner.transform.Rotate(0, RotateDegree, 0);
        }
    }
}

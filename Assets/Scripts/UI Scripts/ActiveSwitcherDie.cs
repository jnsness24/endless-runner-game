﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveSwitcherDie : MonoBehaviour {

    public GameObject menu;

    private GameObject player;
    private PlayerMove moveScript;

    public Text YourScore;
    public Text Highscore;

    private String highScoreNameInternal;

    private int maxValue;
    private int playerValue;

    private bool newRecord = false;


    void Start()
    {
        player = GameObject.Find("SimpleCorgi");
        moveScript = player.GetComponent<PlayerMove>();

    }

    private void Update()
    {
        checkUI();

    }

    private void checkUI()
    {
        if (newRecord)
        {
            YourScore.text = "Your Score - " + moveScript.score.ToString() + " (New Record!!!)";
        }
        else
        {
            YourScore.text = "Your Score - " + moveScript.score.ToString();
        }

        Highscore.text = "Highscore - " + maxValue;
    }

    public void showDieScreen()
    {
        menu.SetActive(true);
        manipulateTime(0.0F);

        playerValue = moveScript.score;

        maxValue = PlayerPrefs.GetInt("highscore", 0);
        savePlayerPrefs();
    }

    private void savePlayerPrefs()
    {
        if (playerValue > maxValue)
        {
            PlayerPrefs.SetInt("highscore", playerValue);
            newRecord = true;
        }
    }


    public void manipulateTime(float timeSpeed)
    {
        Time.timeScale = timeSpeed;
    }

}

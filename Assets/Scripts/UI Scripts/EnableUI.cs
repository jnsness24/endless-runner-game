﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableUI : MonoBehaviour {

    public float sec = 5;
    private GameObject menu;

	// Use this for initialization
	void Start () {
        menu = GameObject.Find("Menu");
        StartCoroutine(LateCall());
    }

    IEnumerator LateCall()
    {
        yield return new WaitForSeconds(sec);
        Debug.Log("5 seconds waited...");
        menu.transform.GetChild(0).gameObject.SetActive(true);
      
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveSwitcher : MonoBehaviour {

    public GameObject menu;
    
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyDown("escape"))
        {
            menuToggle();
        }
    }

    public void menuToggle()
    {
        if (menu.activeSelf == false)
        {
            menu.SetActive(true);
            manipulateTime(0.0F);
        }
        else
        {
            menu.SetActive(false);
            manipulateTime(1.0F);
        }
    }

    public void manipulateTime(float timeSpeed)
    {
        Time.timeScale = timeSpeed;
    }
}

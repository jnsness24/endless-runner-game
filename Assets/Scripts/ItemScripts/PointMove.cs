﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointMove : MonoBehaviour {


    private float hoverDirection = 1.0f;
    private int hoverCounter = 0;
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0, 1, 0);
        hoverCalculation();
     
        transform.Translate(0, 0.01f *hoverDirection, 0);
        hoverCounter++;
	}

    private void hoverCalculation()
    {
        if (hoverCounter == 75)
        {
            hoverDirection = hoverDirection * -1;
            hoverCounter = 0;
        }

    }
}

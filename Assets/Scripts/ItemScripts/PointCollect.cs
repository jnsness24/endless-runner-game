﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointCollect : MonoBehaviour {

    private static GameObject Player;
    PlayerMove valueScript;

    // Use this for initialization
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        valueScript = Player.GetComponent<PlayerMove>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            valueScript.score++;
            Object.Destroy(this.gameObject);
        }
    }
}

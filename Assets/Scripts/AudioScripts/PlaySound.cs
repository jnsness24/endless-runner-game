﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour {

    public int sound;
    public static GameObject SoundPlayer;
    public static AudioSource[] AudioArray;

    void Start()
    {
        SoundPlayer = GameObject.Find("SoundPlayer_Effects");
        AudioArray = SoundPlayer.GetComponents<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playSoundfile(sound);
        }
    }

    public void playSoundfile(int v)
    {
        AudioArray[v].Play();
    }

}

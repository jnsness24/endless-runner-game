﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMove : MonoBehaviour
{

    //Variables
    private float speed = 15.0F;
    public float gravity = 10000000.0F;
    private Vector3 moveDirection = Vector3.zero;
    private float input = 0.0F;
    private float powerCounter = 0.0F;

    private Animator anim;

    public bool freezeSwitch = false;

    //UI Elements
    public int score = 0;
    public string Name;
    public int lives = 3;
    public Text scoreText;
    public Text liveText;
    public Slider powerSlider;
    

    private CharacterController controller;

    //Slope Sliding
    private Vector3 hitNormal;
    private bool sloping;
    public float slideFriction = 0.3f;
    private int slopeFrameLock = 0;
    private int twoSeconds = 46;
    

    void Start()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        ControlCheck();
        UiCheck();
    }

    private void ControlCheck()
    {
        if (slopeFrameLock < 1)
        {
            sloping = Vector3.Angle(Vector3.up, hitNormal) > controller.slopeLimit;

        }
        else
        {
            sloping = false;
            slopeFrameLock--;
        }

        // For better Control in the Air, an action in Air will increase the gravity so you can land better
        if (!controller.isGrounded)
        {

            if (Input.GetKey("space"))
                moveDirection.y = -20f;
        }


        if (Input.GetKeyUp("space"))
        {
            gravity = 15.0F;
            speed = 15.0F;


        }
            


        //Loads up JumpPower, if a button (or Touch) is pressed
        if (Input.GetKey("space"))
        {
            anim.SetBool("trot", true);
            loadJumpPower();
        }



        //Release the Jump, when Button is released
        if (controller.isGrounded || sloping == true)
        {
            moveDirection = transform.forward * speed;
            moveDirection += transform.right * Input.GetAxis("Horizontal") * 10f;
            //moveDirection += new Vector3(Input.GetAxis("Horizontal")*10f, 0, 0);
            if (Input.GetKeyUp("space"))
            {
                anim.SetBool("trot", false);
                anim.SetTrigger("jump");

                moveDirection.y = 7.5f+powerCounter/5;
                powerCounter = 0.0f;
                
                slopeFrameLock = twoSeconds;
            }


        }
        //Applying gravity to the controller
        moveDirection.y -= gravity * Time.deltaTime;
        //Making the character move
        if (!freezeSwitch)
        {
            controller.Move(moveDirection * Time.deltaTime);
        }
        
    }
    
    private void loadJumpPower()
    {
        //slows down movement to give better feedback
        if (speed > 5F)
        {
            speed = speed - 0.2F;
        }

        //loads up Jumppower per Frame
        if (powerCounter < 50)
            powerCounter++;
        else
            powerCounter = 50;
    }

    private void UiCheck()
    {
        scoreText.text = "Count: " + score.ToString();
        powerSlider.value = powerCounter;
        liveText.text = "Lives: " + lives.ToString();
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        hitNormal = hit.normal;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlayer : MonoBehaviour {

    private static GameObject playerObject;
    private static Transform playerTransform;
    public float angle = -3f;

    void Start()
    {
        playerObject = GameObject.Find("SimpleCorgi");
        playerTransform = playerObject.GetComponent<Transform>();

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartCoroutine(rotatePlayer(new Vector3(0,angle,0), 1));
            Debug.Log(angle);
        }        
    }

    IEnumerator rotatePlayer(Vector3 byAngles, float inTime)
    {
        Quaternion fromAngle = playerTransform.transform.rotation;
        Quaternion toAngle = Quaternion.Euler(transform.eulerAngles + byAngles);

        for (float i = 0f; i < 1; i+= Time.deltaTime/inTime)
            {
                playerTransform.transform.rotation = Quaternion.Lerp(fromAngle, toAngle, i);
                yield return null;
            }
    }
}
